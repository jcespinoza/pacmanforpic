/* 
 * File:   TestMain.cpp
 * Author: Jay C Espinoza
 *
 * Created on September 20, 2014, 1:33 PM
 */
#include <plib.h>
#include "MMB.h"
#include "LCDTerminal.h"

// Configuration bits
#pragma config POSCMOD = XT, FNOSC = PRIPLL, FSOSCEN = ON
#pragma config FPLLIDIV = DIV_2, FPLLMUL = MUL_20, FPLLODIV = DIV_1, FPBDIV = DIV_1
#pragma config OSCIOFNC = ON, CP = OFF, BWP = OFF, PWP = OFF

int main(int argc, char** argv) {
//-------------------------------------------------------------
    // initializations
	MMBInit();          // initialize the MikroE MMB board
    LCDInit();

    //-------------------------------------------------------------
    // Show Splash Screen
    SetColor( BRIGHTRED);
    LCDCenterString( -1, "MikroE PIC32MX4 MMB Test");
    LCDCenterString(  0, "v 1.0");
    SetColor( WHITE);
    LCDCenterString(  2, "(move joystick to start)");
    MMBFadeIn( 250);
    return 0;
}

