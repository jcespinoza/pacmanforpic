#ifndef PACSPRITES_H
#define  PACSPRITES_H
#define pacUpSmall_SIZE     518
extern BITMAP_FLASH pacUpSmall;
#define pacRightSmall_SIZE     518
extern BITMAP_FLASH pacRightSmall;
#define pacLeftSmall_SIZE     518
extern BITMAP_FLASH pacLeftSmall;
#define pacDownSmall_SIZE     518
extern BITMAP_FLASH pacDownSmall;
#define ghost_SIZE     518
extern BITMAP_FLASH ghost;
#define fruitSmall_SIZE     518
extern BITMAP_FLASH fruitSmall;
#define empty_SIZE     518
extern BITMAP_FLASH empty;
#endif