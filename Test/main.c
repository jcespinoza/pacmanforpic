#include <plib.h>
#include <stdlib.h>
#include <stdio.h>
#include "MMB.h"
#include "Graphics/Graphics.h"
#include "LCDTerminal.h"
#include "MDD File System/FSIO.h"
#include "SerialFLASH.h"
#include "pacSprites.h"

#define GAME_NOT_STARTED 0
#define GAME_PLAYING 1
#define GAME_FINISHED 2
#define USE_PICTURE
#define FRUIT_ARRAY_SIZE 5
#define ENEMIES_ARRAY_SIZE 5
#define pacSpriteSize 16
#define pacSpriteHalfSize 8

// Configuration bits 
#pragma config POSCMOD = XT, FNOSC = PRIPLL, FSOSCEN = ON
#pragma config FPLLIDIV = DIV_2, FPLLMUL = MUL_20, FPLLODIV = DIV_1, FPBDIV = DIV_1
#pragma config OSCIOFNC = ON, CP = OFF, BWP = OFF, PWP = OFF

// fonts
extern const FONT_FLASH TerminalFont;

#define PutPacRight(x,y)    PutImage(x,y,(void*)&pacRightSmall,IMAGE_NORMAL);
#define PutPacLeft(x,y)     PutImage(x,y,(void*)&pacLeftSmall,IMAGE_NORMAL);
#define PutPacUp(x,y)       PutImage(x,y,(void*)&pacUpSmall,IMAGE_NORMAL);
#define PutPacDown(x,y)     PutImage(x,y,(void*)&pacDownSmall,IMAGE_NORMAL);

//enum for sprite type. An image is chosen according to the type
#define FRUIT_TYPE  1
#define ENEMY_TYPE  2

//enum for sprite states
#define EATED       1
#define NOT_EATED   0

//Aditional direction definitions
#define DIR_UP      0
#define DIR_DOWN    1
#define DIR_LEFT    2
#define DIR_RIGHT   3

#define REFRESH_RATE    50  //Time in miliseconds to wait for the next iteration in main loop()
#define MAX_STEPS       15  //Maximum number of steps before a direction change in moving enemies

/******************************************/
//CODE FOR COLLISION DETECTION by Ivan Deras
//Rectangle struct for collision detection
typedef struct Rectangle {
    unsigned int x1, y1, x2, y2;
}Rectangle_t;

#define PointInRectangle(x, y, x1, y1, x2, y2)  ((( (x) >= (x1)) && ((y) >= (y1))) && (((x) <= (x2)) && ((y) <= (y2))))

int Collide(Rectangle_t *r1, Rectangle_t *r2)
{
    return (PointInRectangle(r1->x1, r1->y1, r2->x1, r2->y1, r2->x2, r2->y2) ||
            PointInRectangle(r1->x2, r1->y2, r2->x1, r2->y1, r2->x2, r2->y2) ||
            PointInRectangle(r1->x1, r1->y2, r2->x1, r2->y1, r2->x2, r2->y2) ||
            PointInRectangle(r1->x2, r1->y1, r2->x1, r2->y1, r2->x2, r2->y2));
}
//END OF CODE FOR COLLISION DETECTION
/**************************************/

//define sprites for use in paint event
typedef struct sprites{
    int x;
    int y;
    int type;
    int eated;
    int direction;
} sprite;

/*Generate random positions for sprites*/
void createSprites(sprite* spriteArray, int desiredSprites, int spriteType){
    int i;
    for(i = 0; i < desiredSprites; i++){
        sprite newlyCreatedSprite = {
            generateRandomNumber(0, 38)*pacSpriteHalfSize,
            generateRandomNumber(0, 28)*pacSpriteHalfSize,
            spriteType,
            NOT_EATED,
            generateRandomNumber(DIR_UP, DIR_RIGHT)
        };
        spriteArray[i] = newlyCreatedSprite;
    }
}

/*Methods to move sprite*/
void moveLeft(sprite* spriteObject){
    //check boundaries first
    if(spriteObject->x > pacSpriteHalfSize){
        //change coordinates value if conditions are valid
        spriteObject->x -= pacSpriteHalfSize;
    }
}
void moveRight(sprite* spriteObject){
    if(spriteObject->x <= GetMaxX() - pacSpriteSize){
        spriteObject->x += pacSpriteHalfSize;
    }
}
void moveUp(sprite* spriteObject){
    if(spriteObject->y > pacSpriteSize){
        spriteObject->y -= pacSpriteHalfSize;
    }
}
void moveDown(sprite* spriteObject){
    if(spriteObject->y <= GetMaxY() - pacSpriteSize){
        spriteObject->y += pacSpriteHalfSize;
    }
}

int CollidesWithAnySprite(sprite* otherSprites, int collectionSize, sprite* targetSprite){
    int i;
    Rectangle_t targetSpriteRectangle = { targetSprite->x,  targetSprite->y,  targetSprite->x + pacSpriteSize,  targetSprite->y + pacSpriteSize};
    for(i = 0; i < collectionSize; i++){
        if(&otherSprites[i] != targetSprite){
            Rectangle_t otherSpriteRectangle = {otherSprites[i].x, otherSprites[i].y, otherSprites[i].x + pacSpriteSize, otherSprites[i].y + pacSpriteSize};
            if(otherSprites[i].eated != EATED && Collide(&targetSpriteRectangle, &otherSpriteRectangle)){
                return 1;
            }
        }
    }
    return 0;
}

void forceDirectionChange(sprite* spriteObject){
    int oldDirection = spriteObject->direction;
    do{
        spriteObject->direction = generateRandomNumber(DIR_UP, DIR_RIGHT+1);
    }while(oldDirection == spriteObject->direction);
}

void paintFruits(sprite* fruits){
    int i = 0;
    for(i = 0; i < FRUIT_ARRAY_SIZE; i++){
        if(!fruits[i].eated)
            PutImage(fruits[i].x, fruits[i].y, (void*)&fruitSmall, IMAGE_NORMAL);
    }
}

void paintPacMan(int oldX, int oldY, int newX, int newY, int direction){
    if(oldX == newX && oldY == newY) return;
    PutImage(oldX, oldY, (void*)&empty, IMAGE_NORMAL);
    switch(direction){
        case JOY_UP:      PutPacUp(newX, newY);     break;
        case JOY_LEFT:    PutPacLeft(newX,newY);    break;
        case JOY_RIGHT:   PutPacRight(newX,newY);   break;
        case JOY_DOWN:    PutPacDown(newX,newY);    break;
    }
}

void paintEnemies(sprite* enemies, int count){
    int i = 0;
    for(i = 0; i < ENEMIES_ARRAY_SIZE; i++){
        if(!enemies[i].eated){
            PutImage(enemies[i].x, enemies[i].y, (void*)&empty, IMAGE_NORMAL);
            if(CollidesWithAnySprite(enemies, ENEMIES_ARRAY_SIZE, &enemies[i])){
                forceDirectionChange(&enemies[i]);
            }
            if(count == MAX_STEPS){
                forceDirectionChange(&enemies[i]);
            }
            switch(enemies[i].direction){
                case DIR_LEFT: moveLeft(&enemies[i]); break;
                case DIR_RIGHT: moveRight(&enemies[i]); break;
                case DIR_UP: moveUp(&enemies[i]); break;
                case DIR_DOWN: moveDown(&enemies[i]); break;
            }
            PutImage(enemies[i].x, enemies[i].y, (void*)&ghost, IMAGE_NORMAL);
        }
    }
}

void checkPacManCollisions(sprite* fruits, sprite* enemies, int positionX, int positionY, int *score){
    int i = 0;
    Rectangle_t pacManRectangle = {positionX, positionY, positionX+pacSpriteSize, positionY+pacSpriteSize};
    //check if pacman collides with a fruit
    for(i = 0; i < FRUIT_ARRAY_SIZE; i++){
        Rectangle_t fruitRectangle = {fruits[i].x, fruits[i].y, fruits[i].x + pacSpriteSize, fruits[i].y + pacSpriteSize};
        //check only if the fruit has not been eated yet
        if(fruits[i].eated != EATED && Collide(&pacManRectangle, &fruitRectangle)){
            fruits[i].eated = EATED;
            *score += 1;
            //force a clear in order to display the score at the top
            LCDClear();
        }
    }
    //check if pacman collides with a ghost
    for(i = 0; i < ENEMIES_ARRAY_SIZE; i++){
        Rectangle_t enemieRectangle = {enemies[i].x, enemies[i].y, enemies[i].x + pacSpriteSize, enemies[i].y + pacSpriteSize};
        if(enemies[i].eated != EATED && Collide(&pacManRectangle, &enemieRectangle)){
            enemies[i].eated = EATED;
            *score += 1;
            LCDClear();
        }
    }
}

int generateRandomNumber(int startRange, int endRange){
    return rand()%endRange + startRange;
}

void showScoreHeader(int score){
    char buffer[4] = "   ";
    sprintf(buffer, "Score %d", score);
    LCDCenterString(-6, buffer);
}

//Simulate a halt in the program until the SELECT key is pressed
void waitForSelectToBePressed(){
    do{}while(MMBGetKey() != JOY_SELECT);
}

int main( void)
{
    int gameState = GAME_NOT_STARTED;
    int gameScore = 0;
    int stepsCount = 0;

    //array of fruits
    sprite fruitsCollection[FRUIT_ARRAY_SIZE];
    //array of enemies
    sprite enemiesCollection[ENEMIES_ARRAY_SIZE];
    
    //-------------------------------------------------------------
    // initializations
	MMBInit();          // initialize the MikroE MMB board
        LCDInit();
    
    int pacmanPosX = 0;
    int pacmanPosY = 0;
    int oldPacmanX = 0;
    int oldPacmanY = 0;
    unsigned int timeCount = 0;
    // main loop
    while( 1 )
    {
        //seed the random number generator
        srand(timeCount);
        if(gameState == GAME_NOT_STARTED){
            //-------------------------------------------------------------
            // Show Splash Screen
            SetColor( BRIGHTCYAN);
            LCDCenterString( -1, "MikroE PacByte Test");
            LCDCenterString(  0, "v 1.0");
            SetColor( WHITE);
            LCDCenterString(  2, "[Press Select to Start]");
            MMBFadeIn( 250 );
            // End Splash Screen

            //reset variables in case this is another game
            createSprites(enemiesCollection, ENEMIES_ARRAY_SIZE, ENEMY_TYPE);
            createSprites(fruitsCollection, FRUIT_ARRAY_SIZE, FRUIT_TYPE);
            pacmanPosX = 0;
            timeCount = 0;
            pacmanPosY = pacSpriteSize;
            oldPacmanX = 0;
            oldPacmanY = 0;
            gameScore = 0;
            stepsCount = 0;
            gameState = GAME_PLAYING;
            waitForSelectToBePressed();
            LCDClear();
        }
        else if(gameState == GAME_PLAYING){
            timeCount++;
            //store old position to paint empty square
            oldPacmanX = pacmanPosX;
            oldPacmanY = pacmanPosY;
            showScoreHeader(gameScore);
            //paint the sprites
            paintFruits(fruitsCollection);
            paintEnemies(enemiesCollection, stepsCount);
            //increment the number of loops since last refresh
            //this tells the enemies whether to change direction or not
            stepsCount = (stepsCount==MAX_STEPS)?0:stepsCount+1;
            //Check the key currenty pressed
            int key = MMBReadKey();
            switch(key){
                case JOY_UP:
                    //validate that it doesn't go out of the screen
                    if(pacmanPosY >= pacSpriteSize){
                        //sprites move half their total size
                        pacmanPosY -= pacSpriteHalfSize;
                    }
                    break;
                case JOY_LEFT:
                    if(pacmanPosX >= pacSpriteHalfSize){
                        pacmanPosX -= pacSpriteHalfSize;
                    }
                    break;
                case JOY_RIGHT:
                    if(pacmanPosX <= GetMaxX()-pacSpriteSize){
                        pacmanPosX += pacSpriteHalfSize;
                    }
                    break;
                case JOY_DOWN:
                    if(pacmanPosY <= GetMaxY()-pacSpriteSize){
                        pacmanPosY += pacSpriteHalfSize;
                    }
                    break;
                case JOY_SELECT:
                    LCDClear();
                    pacmanPosX = 0;
                    pacmanPosY = pacSpriteSize;
                    key = JOY_RIGHT;
                    break;
                default:
                    break;
            }
            checkPacManCollisions(fruitsCollection, enemiesCollection, pacmanPosX, pacmanPosY, &gameScore);
            paintPacMan(oldPacmanX, oldPacmanY, pacmanPosX, pacmanPosY, key);
            if(gameScore == FRUIT_ARRAY_SIZE + ENEMIES_ARRAY_SIZE)
                gameState = GAME_FINISHED;
            DelayMs(REFRESH_RATE);
        }
        else if(gameState == GAME_FINISHED){
            LCDClear();
            SetColor( BRIGHTCYAN);
            LCDCenterString( -1, "You have completed the game");
            LCDCenterString(  0, "Well Done!");
            SetColor( WHITE);
            LCDCenterString(  2, "[Press Select to Continue]");
            waitForSelectToBePressed();
            LCDClear();
            gameState = GAME_NOT_STARTED;
        }
        // clear screen and start from the top
    } // main loop
} // main




